import "./CardList.scss";
import Card from "./Card";
import TableCard from "./TableCard";
import React, { useContext } from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import { SwitchContext } from "./Context/SwitchContext";
import { FaTh, FaTable } from "react-icons/fa";

const CardList = () => {
  const dataList = useSelector((state) => state.dataList);
  const { isTableMode, setIsTableMode } = useContext(SwitchContext);
  const toggleView = () => {
    setIsTableMode(!isTableMode);
  };

  return (
    <div className="card-list container">
      <div className="toggle-buttons m-3">
        <button
          className={`toggle-button btn btn-outline-secondary ${
            isTableMode ? "" : "active"
          }`}
          onClick={toggleView}
        >
          <FaTh />
        </button>
        <button
          className={`toggle-button btn btn-outline-secondary ${
            isTableMode ? "active" : ""
          }`}
          onClick={toggleView}
        >
          <FaTable />
        </button>
      </div>

      {isTableMode ? (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>URL</th>
              <th>Color</th>
            </tr>
          </thead>
          <tbody>
            {dataList.map((card, index) => (
              <TableCard
                key={index}
                name={card.name}
                price={card.price}
                url={card.url}
                color={card.color}
                index={index}
              />
            ))}
          </tbody>
        </table>
      ) : (
        <div className="card-list container">
          {dataList.map((card, index) => (
            <Card
              key={index}
              name={card.name}
              price={card.price}
              url={card.url}
              color={card.color}
              index={index}
            />
          ))}
        </div>
      )}
    </div>
  );
};
CardList.propTypes = {
  dataList: PropTypes.array.isRequired,
};

export default CardList;
