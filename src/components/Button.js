import React from "react";
import "./Button.scss";

const Button = ({ backgroundColor, text, onClick, colorText }) => {
  return (
    <button
      style={{ background: backgroundColor, color: colorText }}
      className="btn btn-outline-success btn-add"
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
