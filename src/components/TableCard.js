import React from "react";
import {
  addToFavorites,
  removeFromFavorites,
  addToCart,
  openModal,
  closeModal,
} from "../redux/actions";
import Button from "./Button";
import Modal from "./Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faStar as farStar } from "@fortawesome/free-regular-svg-icons";
import { useDispatch, useSelector } from "react-redux";

const TableCard = ({ name, price, url, color, index }) => {
  const favoritesList = useSelector((state) => state.favoritesList);

  const isFavorite = favoritesList.some(
    (item) => item.name === name && item.price === price
  );

  const isModal = useSelector((state) => state.isModal === index);

  const dispatch = useDispatch();

  const toggleFavorite = (e) => {
    e.preventDefault();
    if (isFavorite) {
      const itemToRemove = favoritesList.findIndex(
        (item) => item.name === name && item.price === price
      );
      dispatch(removeFromFavorites(itemToRemove));
    } else {
      dispatch(addToFavorites(name, price, url, color));
    }
  };

  const handleClick = () => {
    dispatch(openModal(index));
  };

  const handleClose = () => {
    dispatch(closeModal());
  };

  const handleAddToCart = () => {
    dispatch(addToCart({ name, price, url, color, index }));
    dispatch(closeModal());
  };

  return (
    <tr>
      <td>
        {name}{" "}
        <div className="favorite-icon">
          <a href="#!" onClick={toggleFavorite}>
            <FontAwesomeIcon
              icon={isFavorite ? faStar : farStar}
              className={isFavorite ? "star-icon selected" : "star-icon"}
            />
          </a>
        </div>
      </td>
      <td>{price} $</td>
      <td>
        <img src={url} alt="" />{" "}
      </td>
      <td>
        {color}
        <br />{" "}
        <Button
          text={"Add to cart"}
          onClick={handleClick}
          colorText={"black"}
        />
      </td>

      {isModal && (
        <Modal
          header="Do you want to add this wine to your cart?"
          closeButton={true}
          action="AddCard"
          onClose={handleClose}
          btnOne="Ok"
          btnSecond="Cancel"
          onClick={handleAddToCart}
        />
      )}
    </tr>
  );
};

export default TableCard;
