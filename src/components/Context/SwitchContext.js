import React, { createContext, useState } from "react";

const SwitchContext = createContext();

const SwitchProvider = (props) => {
  const [isTableMode, setIsTableMode] = useState(false);

  return (
    <SwitchContext.Provider value={{ isTableMode, setIsTableMode }}>
      {props.children}
    </SwitchContext.Provider>
  );
};

export { SwitchContext, SwitchProvider };
