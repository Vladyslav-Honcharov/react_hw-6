import React, { useContext } from "react";
import Header from "./components/Header";
import CardList from "./components/CardList";
import { SwitchProvider } from "./components/Context/SwitchContext";

const App = () => {
  return (
    <>
      <Header />
      <SwitchProvider>
        <CardList />
      </SwitchProvider>
    </>
  );
};

export default App;
