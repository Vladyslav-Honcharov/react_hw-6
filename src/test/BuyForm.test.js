import React from "react";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

import { BuyForm } from "../components/Form";

const mockStore = configureStore([]);

test("BuyForm correct show", () => {
  const store = mockStore({});

  const component = renderer.create(
    <Provider store={store}>
      <BuyForm />
    </Provider>
  );

  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
