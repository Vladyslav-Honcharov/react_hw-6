import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import store from "../redux/store"; // Import your Redux store
import Header from "../components/Header";

test("Header component renders correctly", () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router>
        <Header />
      </Router>
    </Provider>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
