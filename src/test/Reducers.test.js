import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  CLEAR_CART,
  SET_DATA_LIST,
  ADD_TO_FAVORITES,
  REMOVE_FROM_FAVORITES,
} from "../redux/type";
import { cartListReducer } from "../redux/cartListReducer";
import dataListReducer from "../redux/dataListReducer";
import { favoritesListReducer } from "../redux/favoritesListReducer";
import {
  loadDataFromLocalStorage,
  saveDataToLocalStorage,
} from "../redux/localStorageActions";

import "@testing-library/jest-dom/extend-expect";

describe("cartListReducer", () => {
  test("add item to cart", () => {
    const initialState = [];
    const action = {
      type: ADD_TO_CART,
      payload: { id: 1, name: "Product 1" },
    };

    const newState = cartListReducer(initialState, action);

    expect(newState).toEqual([{ id: 1, name: "Product 1" }]);
  });

  test("remove item from  cart", () => {
    const initialState = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
      { id: 3, name: "Product 3" },
    ];
    const action = {
      type: REMOVE_FROM_CART,
      payload: 1,
    };

    const newState = cartListReducer(initialState, action);

    expect(newState).toEqual([
      { id: 1, name: "Product 1" },
      { id: 3, name: "Product 3" },
    ]);
  });

  test("clear  cart", () => {
    const initialState = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
      { id: 3, name: "Product 3" },
    ];
    const action = {
      type: CLEAR_CART,
    };

    const newState = cartListReducer(initialState, action);

    expect(newState).toEqual([]);
  });

  test("return the current state for unknown action type", () => {
    const initialState = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
    ];
    const action = {
      type: "UNKNOWN_ACTION",
    };

    const newState = cartListReducer(initialState, action);

    expect(newState).toEqual(initialState);
  });
});

describe("dataListReducer", () => {
  test("should return the initial state", () => {
    const initialState = [];
    const action = {};
    const newState = dataListReducer(initialState, action);
    expect(newState).toEqual(initialState);
  });

  test("should handle SET_DATA_LIST action", () => {
    const initialState = [];
    const data = [1, 2, 3];
    const action = {
      type: SET_DATA_LIST,
      payload: data,
    };
    const newState = dataListReducer(initialState, action);
    expect(newState).toEqual(data);
  });
});

describe("favoritesListReducer", () => {
  test("add item to favorites list", () => {
    const initialState = [];
    const itemToAdd = { id: 1, name: "Product A" };
    const action = {
      type: ADD_TO_FAVORITES,
      payload: itemToAdd,
    };

    const newState = favoritesListReducer(initialState, action);

    expect(newState).toEqual([itemToAdd]);
  });

  test("remove item from favorites list", () => {
    const initialState = [
      { id: 1, name: "Product A" },
      { id: 2, name: "Product B" },
    ];
    const indexToRemove = 1;
    const action = {
      type: REMOVE_FROM_FAVORITES,
      payload: indexToRemove,
    };

    const newState = favoritesListReducer(initialState, action);

    expect(newState).toEqual([{ id: 1, name: "Product A" }]);
  });

  test("return the current state for unknown action", () => {
    const initialState = [
      { id: 1, name: "Product A" },
      { id: 2, name: "Product B" },
    ];
    const action = {
      type: "UNKNOWN_ACTION",
      payload: {},
    };

    const newState = favoritesListReducer(initialState, action);

    expect(newState).toEqual(initialState);
  });
});

describe("localStorage", () => {
  beforeEach(() => {
    localStorage.clear();
  });

  test("load data from localStorage", () => {
    const testData = { key: "value" };
    const serializedTestData = JSON.stringify(testData);
    localStorage.setItem("reduxState", serializedTestData);

    const loadedData = loadDataFromLocalStorage();

    expect(loadedData).toEqual(testData);
  });

  test("save to localStorage", () => {
    const testData = { key: "value" };

    saveDataToLocalStorage(testData);

    const savedData = localStorage.getItem("reduxState");
    const parsedSavedData = JSON.parse(savedData);

    expect(parsedSavedData).toEqual(testData);
  });
});
