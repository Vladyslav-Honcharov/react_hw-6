import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Button from "../components/Button";
import "@testing-library/jest-dom/extend-expect";

describe("Button component", () => {
  test("renders button with provided text and background color", () => {
    const buttonText = "Add to cart";
    const backgroundColor = "yellow";
    const onClick = jest.fn();

    const { getByText } = render(
      <Button
        backgroundColor={backgroundColor}
        text={buttonText}
        onClick={onClick}
      />
    );

    const buttonElement = getByText(buttonText);

    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement).toHaveStyle({ background: backgroundColor });
  });

  test("calls onClick function when button is clicked", () => {
    const onClick = jest.fn();

    const { getByText } = render(
      <Button backgroundColor="blue" text="Add to cart" onClick={onClick} />
    );

    const buttonElement = getByText("Add to cart");

    fireEvent.click(buttonElement);

    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
