import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import Modal from "../components/Modal";

describe("Modal component", () => {
  test("renders modal with header and close button", () => {
    const headerText = "Header";
    const onClose = jest.fn();

    const { getByText, getByRole } = render(
      <Modal header={headerText} closeButton onClose={onClose} />
    );

    const headerElement = getByText(headerText);
    const closeButtonElement = getByRole("button", { name: "X" });

    expect(headerElement).toBeInTheDocument();
    expect(closeButtonElement).toBeInTheDocument();

    fireEvent.click(closeButtonElement);

    expect(onClose).toHaveBeenCalledTimes(1);
  });

  test("renders modal with buttons and calls onClick functions", () => {
    const btnOneText = "Button One";
    const btnTwoText = "Button Two";
    const onClickFn = jest.fn();

    const { getByText } = render(
      <Modal
        header="Modal Header"
        closeButton
        onClose={() => {}}
        btnOne={btnOneText}
        btnSecond={btnTwoText}
        onClick={onClickFn}
      />
    );

    const btnOneElement = getByText(btnOneText);
    const btnTwoElement = getByText(btnTwoText);

    fireEvent.click(btnOneElement);
    fireEvent.click(btnTwoElement);

    expect(onClickFn).toHaveBeenCalledTimes(1);
    expect(onClickFn).toHaveBeenCalledTimes(1);
  });
});
